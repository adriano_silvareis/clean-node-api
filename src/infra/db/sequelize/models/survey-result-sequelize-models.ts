import { Table, Column, Model, DataType, ForeignKey, BelongsTo } from 'sequelize-typescript'

import { SurveyResultModel } from '@/domain/models/survey-result'

import { AccountSequelizeModels } from './account-sequelize-models'
import { SurveySequelizeModels } from './survey-sequelize-models'

@Table({
  tableName: 'survey_result',
  timestamps: false,
  underscored: true
})
export class SurveyResultSequelizeModels extends Model<SurveyResultSequelizeModels> implements SurveyResultModel {
  public id!: string;

  @ForeignKey(() => SurveySequelizeModels)
  @Column
  public surveyId!: string;

  @ForeignKey(() => AccountSequelizeModels)
  @Column
  public accountId!: string;

  @Column(DataType.TEXT)
  public answer!: string

  @Column(DataType.DATE)
  public date!: Date

  @BelongsTo(() => SurveySequelizeModels, { constraints: false })
  public survey!: SurveySequelizeModels;
}
