import { Table, Column, Model, DataType, AllowNull, ForeignKey, BelongsTo } from 'sequelize-typescript'

import { SurveyAnswerModel } from '@/domain/models/survey'

import { SurveySequelizeModels } from './survey-sequelize-models'

@Table({
  tableName: 'survey_answers',
  timestamps: true,
  underscored: true
})
export class SurveyAnswerSequelizeModels extends Model<SurveyAnswerSequelizeModels> implements SurveyAnswerModel {
  public id!: string;

  @ForeignKey(() => SurveySequelizeModels)
  @Column
  public surveyId!: string;

  @Column(DataType.TEXT)
  public image!: string;

  @AllowNull
  @Column(DataType.TEXT)
  public answer!: string;

  @BelongsTo(() => SurveySequelizeModels, { constraints: false })
  public survey!: SurveySequelizeModels;
}
