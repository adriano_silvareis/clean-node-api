import { Table, Model, Column, DataType, AllowNull } from 'sequelize-typescript'

export interface LogErrorModel {
  id: string
  stack: string
  type?: string
}

@Table({
  tableName: 'log_errors',
  timestamps: true,
  underscored: true
})
export class LogSequelizeModels extends Model<LogSequelizeModels> implements LogErrorModel {
  public id!: string;

  @AllowNull
  @Column(DataType.TEXT)
  public stack!: string;

  @AllowNull
  @Column(DataType.DATE)
  public date!: Date;

  @Column(DataType.TEXT)
  public type?: string | undefined;
}
