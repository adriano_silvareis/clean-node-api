import { Table, Column, Model, DataType, AllowNull, HasMany } from 'sequelize-typescript'

import { SurveyModel, SurveyAnswerModel } from '@/domain/models/survey'

import { SurveyAnswerSequelizeModels } from './survey-answer-sequelize-models'

@Table({
  tableName: 'surveys',
  timestamps: true,
  underscored: true
})
export class SurveySequelizeModels extends Model<SurveySequelizeModels> implements SurveyModel {
  public id!: string;

  @AllowNull
  @Column(DataType.TEXT)
  public question!: string;

  @HasMany(() => SurveyAnswerSequelizeModels)
  public answers!: SurveyAnswerModel[];

  public createdAt!: Date;
  public updatedAt!: Date;
}
