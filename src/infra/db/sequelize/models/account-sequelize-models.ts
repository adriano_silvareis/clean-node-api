import { Table, Column, Model, DataType, AllowNull } from 'sequelize-typescript'

import { AccountModel } from '@/domain/models/account'

@Table({
  tableName: 'accounts',
  timestamps: true,
  underscored: true
})
export class AccountSequelizeModels extends Model<AccountSequelizeModels> implements AccountModel {
  public id!: string;

  @AllowNull
  @Column(DataType.TEXT)
  public name!: string;

  @AllowNull
  @Column(DataType.TEXT)
  public email!: string;

  @AllowNull
  @Column(DataType.TEXT)
  public password!: string;

  @Column(DataType.TEXT)
  public accessToken!: string;

  @Column(DataType.TEXT)
  public role!: string;
}
