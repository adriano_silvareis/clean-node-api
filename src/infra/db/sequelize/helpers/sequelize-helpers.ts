import { Sequelize } from 'sequelize-typescript'

import { env } from '@/main/config'

interface ISequelizeHelper {
  client: Sequelize
  uri: string
  connect (): void
  desconnect (): void
}

export class SequelizeHelper implements ISequelizeHelper {
  private static instance?: SequelizeHelper;

  public client!: Sequelize

  private constructor (public readonly uri: string) {
    this.uri = uri
  }

  connect (): void {
    this.client = this.client ?? new Sequelize(this.uri)
  }

  desconnect (): void {
    SequelizeHelper.instance = undefined
  }

  public static getInstance (uri?: string): SequelizeHelper {
    if (!SequelizeHelper.instance) {
      SequelizeHelper.instance = new SequelizeHelper(uri ?? env.postgresUrl)
    }

    return SequelizeHelper.instance
  }
}
