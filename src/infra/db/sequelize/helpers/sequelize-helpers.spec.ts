
import { env } from '@/main/config'

import { SequelizeHelper } from './sequelize-helpers'

describe('SequelizeHelper', () => {
  function makeSut (): SequelizeHelper {
    return SequelizeHelper.getInstance(env.postgresUrl)
  }

  test('Should have client with if sut is connect', () => {
    const sut = makeSut()
    sut.connect()
    expect(sut.client).toBeTruthy()
  })

  test('Should create a connect if sut is not connected', () => {
    const sut = makeSut()
    expect(sut.client).toBeTruthy()
  })

  test('should take standard url when no one is provided', () => {
    makeSut().desconnect()
    const sut = SequelizeHelper.getInstance()
    sut.connect()
    expect(sut.client).toBeTruthy()
  })

  test('should use already created instance of sequelize when requested a new connection', () => {
    const sut = makeSut()
    sut.connect()
    sut.connect()
    expect(sut.client).toBeTruthy()
  })
})
