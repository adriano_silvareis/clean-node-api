import { Sequelize } from 'sequelize'

import { SequelizeHelper } from '@/infra/db/sequelize/helpers/sequelize-helpers'
import { LogSequelizeModels } from '@/infra/db/sequelize/models'
import { env } from '@/main/config'

import { LogPostgresRepository } from './log-postgres-repository'

function makeSut (): LogPostgresRepository {
  return new LogPostgresRepository()
}
describe('Log Mongo Repository', () => {
  beforeAll(() => {
    const instance = SequelizeHelper.getInstance(env.postgresUrl)
    instance.connect()
    instance.client.addModels([LogSequelizeModels])
  })

  afterEach(async () => {
    await LogSequelizeModels.destroy({ where: Sequelize.literal('1=1') })
  })

  test('should create an error log on success', async () => {
    const sut = makeSut()
    await sut.logError('any_error')
    const count = await LogSequelizeModels.count()
    expect(count).toBe(1)
  })
})
