import { LogErrorRepository } from '@/data/protocols/db/log/log-error-repository'

import { LogSequelizeModels } from '../../models'

export class LogPostgresRepository implements LogErrorRepository {
  async logError (stack: string): Promise<void> {
    await LogSequelizeModels.create({
      stack
    })
  }
}
