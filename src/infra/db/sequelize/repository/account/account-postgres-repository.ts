import { Op } from 'sequelize'

import { AddAccountRepository } from '@/data/protocols/db/account/add-account-repository'
import { LoadAccountByTokenRepository } from '@/data/protocols/db/account/load-account-by-token-repository'
import { LoadAccountByEmailRepository, UpdateAccessTokenRepository } from '@/data/usecases/account/authentication/db-authentication-protocols'
import { AccountModel } from '@/domain/models/account'
import { AddAccountParams } from '@/domain/usecases/account/add-account'
import { AccountSequelizeModels } from '@/infra/db/sequelize/models'

export class AccountPostgresRepository implements AddAccountRepository, LoadAccountByEmailRepository, LoadAccountByTokenRepository, UpdateAccessTokenRepository {
  async add (accountData: AddAccountParams): Promise<AccountModel> {
    const account = await AccountSequelizeModels.create(accountData)
    return account
  }

  async loadByEmail (email: string): Promise<AccountModel | null> {
    const account = await AccountSequelizeModels.findOne({ where: { email } })
    return account
  }

  async updateAccessToken (id: string, token: string): Promise<void> {
    await AccountSequelizeModels.update({ accessToken: token }, {
      where: { id }
    })
  }

  async loadByToken (token: string, role?: string): Promise<AccountModel | null> {
    const account = await AccountSequelizeModels.findOne({
      where: {
        accessToken: token,
        [Op.or]: [{ role: role ?? null }, { role: 'admin' }]
      }
    })
    return account
  }
}
