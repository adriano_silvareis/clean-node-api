import { Sequelize } from 'sequelize'

import { mockAddAccountParams } from '@/domain/test'
import { SequelizeHelper } from '@/infra/db/sequelize/helpers/sequelize-helpers'
import { AccountSequelizeModels } from '@/infra/db/sequelize/models'
import { env } from '@/main/config'

import { AccountPostgresRepository } from './account-postgres-repository'

describe('Account Postgres Repository', () => {
  beforeAll(() => {
    const instance = SequelizeHelper.getInstance(env.postgresUrl)
    instance.connect()
    instance.client.addModels([AccountSequelizeModels])
  })

  afterEach(async () => {
    await AccountSequelizeModels.destroy({ where: Sequelize.literal('1=1') })
  })

  function makeSut (): AccountPostgresRepository {
    return new AccountPostgresRepository()
  }

  describe('add()', () => {
    test('should return an account on add success', async () => {
      const sut = makeSut()
      const account = await sut.add(mockAddAccountParams())
      expect(account).toBeTruthy()
      expect(account.id).toBeTruthy()
      expect(account.name).toBe('any_name')
      expect(account.email).toBe('any_email@email.com')
      expect(account.password).toBe('any_password')
    })
  })

  describe('LoadByEmail', () => {
    test('should return an account on loadbyEmail success', async () => {
      const sut = makeSut()
      await AccountSequelizeModels.create(mockAddAccountParams())
      const account = await sut.loadByEmail('any_email@email.com')
      expect(account).toBeTruthy()
      expect(account?.id).toBeTruthy()
      expect(account?.name).toBe('any_name')
      expect(account?.email).toBe('any_email@email.com')
      expect(account?.password).toBe('any_password')
    })

    test('should return null if loadByEmail fails', async () => {
      const sut = makeSut()
      const account = await sut.loadByEmail('any_email@email.com')
      expect(account).toBeFalsy()
    })
  })

  describe('updateAccessToken', () => {
    test('should update the accountAccessToken on updateAccessToken success', async () => {
      const sut = makeSut()
      const fakeAccount = await AccountSequelizeModels.create(mockAddAccountParams())
      expect(fakeAccount.accessToken).toBeFalsy()
      await sut.updateAccessToken(fakeAccount.id, 'any_token')
      const account = await AccountSequelizeModels.findOne({ where: { id: fakeAccount.id } })
      expect(account).toBeTruthy()
      expect(account?.accessToken).toBe('any_token')
    })
  })

  describe('LoadByToken', () => {
    test('should return an account on loadbyToken success without role', async () => {
      const sut = makeSut()
      await AccountSequelizeModels.create({
        name: 'any_name',
        email: 'any_email@email.com',
        password: 'any_password',
        accessToken: 'any_token'
      })
      const account = await sut.loadByToken('any_token')
      expect(account).toBeTruthy()
      expect(account?.id).toBeTruthy()
      expect(account?.name).toBe('any_name')
      expect(account?.email).toBe('any_email@email.com')
      expect(account?.password).toBe('any_password')
    })

    test('should return an account on loadbyToken success with admin role', async () => {
      const sut = makeSut()
      await AccountSequelizeModels.create({
        name: 'any_name',
        email: 'any_email@email.com',
        password: 'any_password',
        accessToken: 'any_token',
        role: 'admin'
      })
      const account = await sut.loadByToken('any_token', 'admin')
      expect(account).toBeTruthy()
      expect(account?.id).toBeTruthy()
      expect(account?.name).toBe('any_name')
      expect(account?.email).toBe('any_email@email.com')
      expect(account?.password).toBe('any_password')
    })

    test('should return an account on loadbyToken with if user is admin', async () => {
      const sut = makeSut()
      await AccountSequelizeModels.create({
        name: 'any_name',
        email: 'any_email@email.com',
        password: 'any_password',
        accessToken: 'any_token',
        role: 'admin'
      })
      const account = await sut.loadByToken('any_token')
      expect(account).toBeTruthy()
      expect(account?.id).toBeTruthy()
      expect(account?.name).toBe('any_name')
      expect(account?.email).toBe('any_email@email.com')
      expect(account?.password).toBe('any_password')
    })

    test('should return null on loadbyToken success with invalid role', async () => {
      const sut = makeSut()
      await AccountSequelizeModels.create({
        name: 'any_name',
        email: 'any_email@email.com',
        password: 'any_password',
        accessToken: 'any_token'
      })
      const account = await sut.loadByToken('any_token', 'admin')
      expect(account).toBeFalsy()
    })

    test('should return null if loadByToken fails', async () => {
      const sut = makeSut()
      const account = await sut.loadByToken('any_token')
      expect(account).toBeFalsy()
    })
  })
})
