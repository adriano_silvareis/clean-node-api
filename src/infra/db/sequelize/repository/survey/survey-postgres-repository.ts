import { AddSurveyRepository } from '@/data/protocols/db/survey/add-survey-repository'
import { LoadSurveysRepository } from '@/data/protocols/db/survey/load-survey-repository'
import { LoadSurveyByIdRepository } from '@/data/protocols/db/survey/load-survey-repository-by-id-repository'
import { SurveyModel } from '@/domain/models/survey'
import { AddSurveyParams } from '@/domain/usecases/survey/add-survey'
import { SurveyAnswerSequelizeModels, SurveySequelizeModels } from '@/infra/db/sequelize/models'

export class SurveyPostgresRepository implements AddSurveyRepository, LoadSurveysRepository, LoadSurveyByIdRepository {
  async add (surveyData: AddSurveyParams): Promise<void> {
    const survey = new SurveySequelizeModels(surveyData)
    await survey.save()

    await Promise.all(surveyData.answers.map(async answer => {
      const surveyAnser = new SurveyAnswerSequelizeModels(answer)
      surveyAnser.surveyId = survey.id
      await surveyAnser.save()
    }))
  }

  async loadAll (): Promise<SurveyModel[]> {
    const surveys = await SurveySequelizeModels.findAll({
      include: [
        {
          model: SurveyAnswerSequelizeModels
        }
      ]
    })
    return surveys
  }

  async loadById (id: string): Promise<SurveyModel | null> {
    const survey = await SurveySequelizeModels.findOne({
      where: { id },
      include: [
        {
          model: SurveyAnswerSequelizeModels
        }
      ]
    })
    return survey
  }
}
