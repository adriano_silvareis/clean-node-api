import { Sequelize } from 'sequelize'

import { mockSurveyParams } from '@/domain/test'
import { SequelizeHelper } from '@/infra/db/sequelize/helpers/sequelize-helpers'
import { env } from '@/main/config'

import { SurveyAnswerSequelizeModels, SurveySequelizeModels } from '../../models'
import { SurveyPostgresRepository } from './survey-postgres-repository'

describe('Survey Postgres Repository', () => {
  beforeAll(() => {
    const instance = SequelizeHelper.getInstance(env.postgresUrl)
    instance.connect()
    instance.client.addModels([SurveyAnswerSequelizeModels, SurveySequelizeModels])
  })

  afterEach(async () => {
    await SurveySequelizeModels.destroy({ where: Sequelize.literal('1=1') })
  })

  describe('add()', () => {
    test('should add a survey on success', async () => {
      const sut = makeSut()
      await sut.add(mockSurveyParams())
      const survey = await SurveySequelizeModels.findOne({
        where: { question: 'any_question' }
      })
      expect(survey).toBeTruthy()
    })
  })

  describe('loadAll()', () => {
    test('should load all surveys on success', async () => {
      const sut = makeSut()
      await sut.add({
        question: 'any_question',
        answers: [
          {
            image: 'any_image',
            answer: 'any_answer'
          }
        ]
      })
      await sut.add({
        question: 'other_question',
        answers: [
          {
            image: 'any_image',
            answer: 'other_answer'
          }
        ]
      })

      const surveys = await sut.loadAll()
      expect(surveys.length).toBe(2)
      expect(surveys[0].question).toBe('any_question')
      expect(surveys[1].question).toBe('other_question')
    })
    test('should load all surveys answers on success', async () => {
      const sut = makeSut()
      await sut.add({
        question: 'any_question',
        answers: [
          {
            image: 'any_image',
            answer: 'any_answer'
          }
        ]
      })
      const [survey] = await sut.loadAll()
      console.log(survey)
      expect(survey.question).toBe('any_question')
      expect(survey.answers.length).toBe(1)
      expect(survey.answers[0].answer).toBe('any_answer')
    })
    test('should load empty list', async () => {
      const sut = makeSut()
      const surveys = await sut.loadAll()
      expect(surveys.length).toBe(0)
    })
  })

  describe('loadById()', () => {
    test('should load surveys by id on success', async () => {
      const sut = makeSut()
      await sut.add({
        question: 'any_question',
        answers: [
          {
            image: 'any_image',
            answer: 'any_answer'
          }
        ]
      })
      const [{ id }] = await sut.loadAll()
      const survey = await sut.loadById(id)
      expect(survey).toBeTruthy()
    })
  })
})

function makeSut (): SurveyPostgresRepository {
  return new SurveyPostgresRepository()
}
