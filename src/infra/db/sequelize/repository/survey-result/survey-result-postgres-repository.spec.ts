
import { Sequelize } from 'sequelize'

import { AccountModel } from '@/domain/models/account'
import { SurveyModel, SurveyAnswerModel } from '@/domain/models/survey'
import { SequelizeHelper } from '@/infra/db/sequelize/helpers/sequelize-helpers'
import { env } from '@/main/config'

import { SurveyAnswerSequelizeModels, SurveySequelizeModels, AccountSequelizeModels, SurveyResultSequelizeModels } from '../../models'
import { SurveyResultPostgresRepository } from './survey-result-postgres-repository'

describe('SurveyResultPostgresRepository', () => {
  beforeAll(() => {
    const instance = SequelizeHelper.getInstance(env.postgresUrl)
    instance.connect()
    instance.client.addModels([AccountSequelizeModels, SurveyAnswerSequelizeModels, SurveySequelizeModels, SurveyResultSequelizeModels])
  })

  afterEach(async () => {
    await SurveyResultSequelizeModels.destroy({ where: Sequelize.literal('1=1') })
    await SurveySequelizeModels.destroy({ where: Sequelize.literal('1=1') })
    await AccountSequelizeModels.destroy({ where: Sequelize.literal('1=1') })
  })

  describe('save()', () => {
    test('should add a survey result if its new', async () => {
      const sut = makeSut()
      const survey = await makeSurvey()
      const answer = await makeSurveyAnswer(survey.id)
      const account = await makeAccount()

      const surveyResult = await sut.save({
        surveyId: survey.id,
        accountId: account.id,
        answer: answer.answer,
        date: new Date()
      })
      expect(surveyResult).toBeTruthy()
      expect(surveyResult.id).toBeTruthy()
      expect(surveyResult.answer).toBe(answer.answer)
    })
    test('should update a survey result if its not new', async () => {
      const sut = makeSut()
      const survey = await makeSurvey()
      const answer = await makeSurveyAnswer(survey.id)
      const account = await makeAccount()

      const surveyResult = await sut.save({
        surveyId: survey.id,
        accountId: account.id,
        answer: answer.answer,
        date: new Date()
      })

      const surveyResult2 = await sut.save(surveyResult)
      expect(surveyResult2).toBeTruthy()
      expect(surveyResult2.id).toEqual(surveyResult.id)
      expect(surveyResult2.answer).toBe(answer.answer)
    })
  })
})

function makeSut (): SurveyResultPostgresRepository {
  return new SurveyResultPostgresRepository()
}

async function makeSurveyAnswer (surveyId: string): Promise<SurveyAnswerModel> {
  const answer = await SurveyAnswerSequelizeModels.create({
    surveyId,
    image: 'any_image',
    answer: 'any_answer'
  })
  return answer
}

async function makeSurvey (): Promise<SurveyModel> {
  const question = await SurveySequelizeModels.create({
    question: 'any_question'
  })
  return question
}

async function makeAccount (): Promise<AccountModel> {
  const account = await AccountSequelizeModels.create({
    name: 'any_name',
    email: 'any_email@email.com',
    password: 'any_password',
    accessToken: 'any_token'
  })
  return account
}
