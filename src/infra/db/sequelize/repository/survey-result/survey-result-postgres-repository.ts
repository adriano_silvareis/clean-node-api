import { SaveSurveyResultRepository } from '@/data/protocols/db/survey-result/save-survey-result-repository'
import { SurveyResultModel } from '@/domain/models/survey-result'
import { SaveSurveyResultParams } from '@/domain/usecases/survey-result/save-survey-result'

import { SurveyResultSequelizeModels } from '../../models/survey-result-sequelize-models'

export class SurveyResultPostgresRepository implements SaveSurveyResultRepository {
  async save (surveyData: SaveSurveyResultParams): Promise<SurveyResultModel> {
    const [surveyResult] = await SurveyResultSequelizeModels.findOrBuild({ where: { surveyId: surveyData.surveyId, accountId: surveyData.accountId } })
    surveyResult.answer = surveyData.answer
    surveyResult.date = surveyData.date
    return await surveyResult.save()
  }
}
