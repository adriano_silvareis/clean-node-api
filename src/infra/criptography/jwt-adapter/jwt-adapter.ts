import jwt from 'jsonwebtoken'

import { Decrypter } from '@/data/protocols/criptograph/decrypter'
import { Encrypter } from '@/data/protocols/criptograph/encrypter'

export class JwtAdapter implements Encrypter, Decrypter {
  constructor (private readonly secret: string) {}

  async encrypter (value: string): Promise<string> {
    const accessToken = await jwt.sign({ id: value }, this.secret)
    return accessToken
  }

  async decrypt (token: string): Promise<string> {
    const value: any = await jwt.verify(token, this.secret)
    return value
  }
}
