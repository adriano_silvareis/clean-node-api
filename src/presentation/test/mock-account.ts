import { mockAccountModel } from '@/domain/test'
import { Authentication, AuthenticationParams } from '@/presentation/controllers/auth/login/login-controller-protocols'
import { AddAccountParams, AddAccount, AccountModel } from '@/presentation/controllers/auth/signup/signup-controller-protocols'

import { LoadAccountByToken } from '../middlewares/auth-middleware-protocols'

export function makeAddAccount (): AddAccount {
  class AddAccountStub implements AddAccount {
    async add (account: AddAccountParams): Promise<AccountModel> {
      return Promise.resolve(mockAccountModel())
    }
  }
  return new AddAccountStub()
}

export function mockAuthentication (): Authentication {
  class AuthenticationStub implements Authentication {
    async auth (authentication: AuthenticationParams): Promise<string> {
      return Promise.resolve('any_token')
    }
  }
  return new AuthenticationStub()
}

export function mockLoadAccountBytoken (): LoadAccountByToken {
  class LoadAccountBytokenStub implements LoadAccountByToken {
    async load (accessToken: string, role?: string): Promise<AccountModel> {
      return Promise.resolve(mockAccountModel())
    }
  }
  return new LoadAccountBytokenStub()
}
