import { SurveyModel } from '../../models/survey'

export type AddSurveyParams = Omit<SurveyModel, 'id'>

export interface SurveyAnswer {
  image?: string
  answer: string
}

export interface AddSurvey {
  add (data: AddSurveyParams): Promise<void>
}
