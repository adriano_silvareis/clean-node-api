import { SurveyModel } from '@/domain/models/survey'
import { AddSurveyParams } from '@/domain/usecases/survey/add-survey'

export function mockSurvey (): SurveyModel {
  return {
    id: 'any_id',
    question: 'any_question',
    answers: [
      {
        id: 'any_answer_id',
        surveyId: 'any_id',
        image: 'any_image',
        answer: 'any_answer'
      }
    ],
    createdAt: new Date()
  }
}

export function mockSurveyParams (): AddSurveyParams {
  return {
    question: 'any_question',
    answers: [{
      image: 'any_image',
      answer: 'any_answer'
    }]
  }
}

export function mockSurveys (): SurveyModel[] {
  return [{
    id: 'any_id',
    question: 'any_question',
    answers: [
      {
        id: 'any_answer_id',
        surveyId: 'any_id',
        image: 'any_image',
        answer: 'any_answer'
      }
    ],
    createdAt: new Date()
  }]
}
