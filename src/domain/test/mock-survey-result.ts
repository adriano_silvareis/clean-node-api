import { SurveyResultModel } from '@/domain/models/survey-result'
import { SaveSurveyResultParams } from '@/domain/usecases/survey-result/save-survey-result'

export function mockSurveyResult (): SurveyResultModel {
  return Object.assign({}, mockSurveyResultParams(), { id: 'any_id' })
}

export function mockSurveyResultParams (): SaveSurveyResultParams {
  return {
    accountId: 'any_account_id',
    surveyId: 'any_survey_id',
    answer: 'any_answer',
    date: new Date()
  }
}
