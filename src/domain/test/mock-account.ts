import { AccountModel } from '../models/account'
import { AddAccountParams } from '../usecases/account/add-account'
import { AuthenticationParams } from '../usecases/account/authentication'

export function mockAddAccountParams (): AddAccountParams {
  return {
    name: 'any_name',
    email: 'any_email@email.com',
    password: 'any_password'
  }
}

export function mockAccountModel (): AccountModel {
  return Object.assign({}, mockAddAccountParams(), { id: 'any_id' })
}

export function mockAuthenticationModel (): AuthenticationParams {
  return {
    email: 'any_email@email.com',
    password: 'any_password'
  }
}
