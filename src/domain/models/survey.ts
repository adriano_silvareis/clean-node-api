export interface SurveyModel {
  id: string
  question: string
  answers: SurveyAnswerModel[]
  createdAt?: Date
  updatedAt?: Date
}

export interface SurveyAnswerModel {
  id?: string
  surveyId?: string
  image?: string
  answer: string
}
