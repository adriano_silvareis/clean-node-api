import bcrypt from 'bcrypt'
import { Sequelize } from 'sequelize'
import request from 'supertest'

import { SequelizeHelper } from '@/infra/db/sequelize/helpers/sequelize-helpers'
import { AccountSequelizeModels } from '@/infra/db/sequelize/models'

import { app, env } from '../config'

describe('Login Routes', () => {
  beforeAll(() => {
    const instance = SequelizeHelper.getInstance(env.postgresUrl)
    instance.connect()
    instance.client.addModels([AccountSequelizeModels])
  })

  afterEach(async () => {
    await AccountSequelizeModels.destroy({ where: Sequelize.literal('1=1') })
  })

  describe('POST /SignUp Routes', () => {
    test('should return 200 on signup', async () => {
      await request(app)
        .post('/api/signup')
        .send({
          name: 'Adriano',
          email: 'adriano_silvareis@hotmail.com',
          password: '123',
          passwordConfirmation: '123'
        })
        .expect(200)
    })
  })

  describe('POST /Login Routes', () => {
    test('should return 200 on login', async () => {
      const password = await bcrypt.hash('123', 12)
      await AccountSequelizeModels.create({
        name: 'Adriano',
        email: 'adriano_silvareis@hotmail.com',
        password
      })
      await request(app)
        .post('/api/login')
        .send({
          email: 'adriano_silvareis@hotmail.com',
          password: '123'
        })
        .expect(200)
    })
    test('should return 401 on login', async () => {
      await request(app)
        .post('/api/login')
        .send({
          email: 'adriano_silvareis@hotmail.com',
          password: '123'
        })
        .expect(401)
    })
  })
})
