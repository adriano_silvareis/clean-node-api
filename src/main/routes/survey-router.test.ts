import { sign } from 'jsonwebtoken'
import { Sequelize } from 'sequelize'
import request from 'supertest'

import { SequelizeHelper } from '@/infra/db/sequelize/helpers/sequelize-helpers'
import { SurveyAnswerSequelizeModels, SurveySequelizeModels, AccountSequelizeModels } from '@/infra/db/sequelize/models'

import { app, env } from '../config'

describe('Survey Routes', () => {
  beforeAll(() => {
    const instance = SequelizeHelper.getInstance(env.postgresUrl)
    instance.connect()
    instance.client.addModels([SurveyAnswerSequelizeModels, SurveySequelizeModels, AccountSequelizeModels])
  })

  afterEach(async () => {
    await SurveySequelizeModels.destroy({ where: Sequelize.literal('1=1') })
    await AccountSequelizeModels.destroy({ where: Sequelize.literal('1=1') })
  })

  describe('POST /Survey Routes', () => {
    test('should return 403 on add survey success', async () => {
      await request(app)
        .post('/api/surveys')
        .send({
          question: 'any_question',
          answers: [
            {
              image: 'any_image',
              answer: 'any_answer'
            },
            {
              answer: 'any_answer'
            }
          ]
        })
        .expect(403)
    })
    test('should return 204 on add survey with valid token', async () => {
      const accessToken = await makeAccessToken()
      await request(app)
        .post('/api/surveys')
        .set('x-access-token', accessToken)
        .send({
          question: 'any_question',
          answers: [
            {
              image: 'any_image',
              answer: 'any_answer'
            },
            {
              answer: 'any_answer'
            }
          ]
        })
        .expect(204)
    })
  })

  describe('GET /Surveys Routes', () => {
    test('should return 403 on load without acessToken survey success', async () => {
      await request(app)
        .get('/api/surveys')
        .expect(403)
    })
    test('should return 204 on load with valid acessToken', async () => {
      const accessToken = await makeAccessToken()
      await request(app)
        .get('/api/surveys')
        .set('x-access-token', accessToken)
        .expect(204)
    })
  })
})

async function makeAccessToken (): Promise<string> {
  const account = await AccountSequelizeModels.create({
    name: 'Adriano',
    email: 'adriano_silvareis@hotmail.com',
    password: '123',
    role: 'admin'
  })
  const accessToken = sign({ id: account.id }, env.jwtSecret)
  account.accessToken = accessToken
  await account.save()
  return accessToken
}
