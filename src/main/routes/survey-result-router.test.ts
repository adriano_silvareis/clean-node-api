import { sign } from 'jsonwebtoken'
import { Sequelize } from 'sequelize'
import request from 'supertest'

import { SequelizeHelper } from '@/infra/db/sequelize/helpers/sequelize-helpers'
import { SurveyAnswerSequelizeModels, SurveySequelizeModels, AccountSequelizeModels, SurveyResultSequelizeModels } from '@/infra/db/sequelize/models'

import { app, env } from '../config'

describe('SurveyResult Routes', () => {
  beforeAll(() => {
    const instance = SequelizeHelper.getInstance(env.postgresUrl)
    instance.connect()
    instance.client.addModels([AccountSequelizeModels, SurveyAnswerSequelizeModels, SurveySequelizeModels, SurveyResultSequelizeModels])
  })

  afterEach(async () => {
    await SurveyResultSequelizeModels.destroy({ where: Sequelize.literal('1=1') })
    await SurveySequelizeModels.destroy({ where: Sequelize.literal('1=1') })
    await AccountSequelizeModels.destroy({ where: Sequelize.literal('1=1') })
  })

  describe('PUT /surveys/:surveyId/results', () => {
    test('should return 403 on save survey witout accssToken', async () => {
      await request(app)
        .put('/api/surveys/any_id/results')
        .send({
          answer: 'any_answer'
        })
        .expect(403)
    })

    test('should return 200 on save survey with accssToken', async () => {
      const accessToken = await makeAccessToken()
      const survey = await SurveySequelizeModels.create({
        question!: 'any_question'
      })
      await SurveyAnswerSequelizeModels.create({
        answer: 'any_answer',
        image: 'any_image',
        surveyId: survey.id
      })

      await request(app)
        .put(`/api/surveys/${survey.id}/results`)
        .set('x-access-token', accessToken)
        .send({
          answer: 'any_answer'
        })
        .expect(200)
    })
  })
})

async function makeAccessToken (): Promise<string> {
  const account = await AccountSequelizeModels.create({
    name: 'Adriano',
    email: 'adriano_silvareis@hotmail.com',
    password: '123',
    role: 'admin'
  })
  const accessToken = sign({ id: account.id }, env.jwtSecret)
  account.accessToken = accessToken
  await account.save()
  return accessToken
}
