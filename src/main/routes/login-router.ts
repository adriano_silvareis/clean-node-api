import { Router } from 'express'

import { adaptRouter } from '../adapters/express-route-adapter'
import { makeLoginController } from '../factores/controllers/auth/login/login-controller-factory'
import { makeSignUpController } from '../factores/controllers/auth/signup/signup-controller-factory'

export default (router: Router): void => {
  router.post('/signup', adaptRouter(makeSignUpController()))
  router.post('/login', adaptRouter(makeLoginController()))
}
