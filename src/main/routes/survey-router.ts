import { Router } from 'express'

import { adaptRouter } from '../adapters/express-route-adapter'
import { makeAddSurveyController } from '../factores/controllers/survey/add-survey/add-survey-controller-factory'
import { makeLoadSurveyController } from '../factores/controllers/survey/load-surveys/load-survey-controller-factory'
import { adminAuth } from '../middlewares/admin-auth'
import { auth } from '../middlewares/auth'

export default (router: Router): void => {
  router.post('/surveys', adminAuth, adaptRouter(makeAddSurveyController()))
  router.get('/surveys', auth, adaptRouter(makeLoadSurveyController()))
}
