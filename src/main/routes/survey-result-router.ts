import { Router } from 'express'

import { adaptRouter } from '../adapters/express-route-adapter'
import { makeSaveSurveyResultController } from '../factores/controllers/survey-result/save-survey-result/save-survey-result-controller-factory'
import { auth } from '../middlewares/auth'

export default (router: Router): void => {
  router.put('/surveys/:surveyId/results', auth, adaptRouter(makeSaveSurveyResultController()))
}
