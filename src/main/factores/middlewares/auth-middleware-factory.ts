import { AuthMiddleware } from '@/presentation/middlewares/auth-middleware'
import { Middleware } from '@/presentation/protocols'

import { makeDbLoadAccountByToken } from '../usecase/account/loadAccountByToken/db-load-account-by-token-factory'

export const makeAuthMiddeware = (role?: string): Middleware => {
  return new AuthMiddleware(makeDbLoadAccountByToken(), role)
}
