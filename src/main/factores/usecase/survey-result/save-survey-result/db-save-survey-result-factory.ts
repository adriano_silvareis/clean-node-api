import { DbSaveSurveyResult } from '@/data/usecases/survey-result/save-survey-result/db-save-surve-result'
import { SaveSurveyResult } from '@/domain/usecases/survey-result/save-survey-result'
import { SurveyResultPostgresRepository } from '@/infra/db/sequelize/repository/survey-result/survey-result-postgres-repository'

export const makeDbSaveSurveyResult = (): SaveSurveyResult => {
  const surveyResultPostgresRepository = new SurveyResultPostgresRepository()
  return new DbSaveSurveyResult(surveyResultPostgresRepository)
}
