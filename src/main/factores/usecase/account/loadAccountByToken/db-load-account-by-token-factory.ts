import { DbLoadAccountByToken } from '@/data/usecases/account/load-account-by-token/db-load-account-by-token'
import { LoadAccountByToken } from '@/domain/usecases/account/load-account-by-token'
import { JwtAdapter } from '@/infra/criptography/jwt-adapter/jwt-adapter'
import { AccountPostgresRepository } from '@/infra/db/sequelize/repository/account/account-postgres-repository'
import { env } from '@/main/config'

export const makeDbLoadAccountByToken = (): LoadAccountByToken => {
  const accountPostgresRepository = new AccountPostgresRepository()
  const decrypter = new JwtAdapter(env.jwtSecret)
  return new DbLoadAccountByToken(decrypter, accountPostgresRepository)
}
