import { DbLoadSurveys } from '@/data/usecases/survey/load-surveys/db-load-surveys'
import { LoadSurveys } from '@/domain/usecases/survey/load-surveys'
import { SurveyPostgresRepository } from '@/infra/db/sequelize/repository/survey/survey-postgres-repository'

export const makeDbLoadSurvey = (): LoadSurveys => {
  const surveyPostgresRepository = new SurveyPostgresRepository()
  return new DbLoadSurveys(surveyPostgresRepository)
}
