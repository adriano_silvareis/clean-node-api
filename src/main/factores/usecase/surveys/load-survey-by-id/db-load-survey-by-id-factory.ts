import { DbLoadSurveyById } from '@/data/usecases/survey/load-surveys-by-id/db-load-surveys-by-id'
import { LoadSurveysById } from '@/domain/usecases/survey/load-surveys-by-id'
import { SurveyPostgresRepository } from '@/infra/db/sequelize/repository/survey/survey-postgres-repository'

export const makeDbLoadSurveyById = (): LoadSurveysById => {
  const surveyPostgresRepository = new SurveyPostgresRepository()
  return new DbLoadSurveyById(surveyPostgresRepository)
}
