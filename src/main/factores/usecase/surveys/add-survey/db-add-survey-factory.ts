import { DbAddSurvey } from '@/data/usecases/survey/add-survey/db-add-surve'
import { AddSurvey } from '@/domain/usecases/survey/add-survey'
import { SurveyPostgresRepository } from '@/infra/db/sequelize/repository/survey/survey-postgres-repository'

export const makeDbAddSurvey = (): AddSurvey => {
  const surveyPostgresRepository = new SurveyPostgresRepository()
  return new DbAddSurvey(surveyPostgresRepository)
}
