import { makeLogControllerDecorator } from '@/main/factores/decorators/log-controller-decorator-factory'
import { SignUpController } from '@/presentation/controllers/auth/signup/signup-controller'
import { Controller } from '@/presentation/protocols'

import { makeDbAddAccount } from '../../../usecase/account/add-account/db-add-account-factory'
import { makeDbAuthentication } from '../../../usecase/account/authentication/db-authentication-factory'
import { makeSignUpValidation } from './signup-validation-factory'

export const makeSignUpController = (): Controller => {
  const signUpController = new SignUpController(makeDbAddAccount(), makeSignUpValidation(), makeDbAuthentication())
  return makeLogControllerDecorator(signUpController)
}
