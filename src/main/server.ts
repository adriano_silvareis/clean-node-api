import 'module-alias/register'
import { app, env, database } from './config'

database.init(env.postgresUrl)
app.listen(env.port, () => console.log(`Server running at http://localhost:${env.port.toString()}`))
