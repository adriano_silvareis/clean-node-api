export const addSurveyAnswerParamsSchema = {
  type: 'object',
  properties: {
    image: { type: 'string' },
    answer: { type: 'string' }
  }
}
