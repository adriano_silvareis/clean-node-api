export const surveyResultSchema = {
  type: 'object',
  properties: {
    answer: {
      type: 'string'
    }
  }
}
