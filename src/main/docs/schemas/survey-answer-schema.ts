export const surveyAnswerSchema = {
  type: 'object',
  properties: {
    id: { type: 'string' },
    surveyId: { type: 'string' },
    image: { type: 'string' },
    answer: { type: 'string' }
  }
}
