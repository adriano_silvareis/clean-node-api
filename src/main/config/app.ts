import express from 'express'

import { middlewares as setupMiddlewares } from './middlewares'
import { routes as setupRoutes } from './routes'
import { swagger as setupSwagger } from './swagger'

export const app = express()

setupSwagger(app)
setupMiddlewares(app)
setupRoutes(app)
