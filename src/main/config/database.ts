import { SequelizeHelper } from '@/infra/db/sequelize/helpers/sequelize-helpers'
import { AccountSequelizeModels, LogSequelizeModels, SurveyAnswerSequelizeModels, SurveySequelizeModels, SurveyResultSequelizeModels } from '@/infra/db/sequelize/models'

export function init (uri: string): SequelizeHelper {
  const sequelize = SequelizeHelper.getInstance(uri)
  sequelize.connect()

  sequelize.client.addModels([
    AccountSequelizeModels,
    LogSequelizeModels,
    SurveyAnswerSequelizeModels,
    SurveySequelizeModels,
    SurveyResultSequelizeModels
  ])

  return sequelize
}
