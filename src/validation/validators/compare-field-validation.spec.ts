import { InvalidParamError } from '@/presentation/error'

import { CompareFieldValidation } from './compare-field-validation'

function makeSut (): CompareFieldValidation {
  return new CompareFieldValidation('field', 'fieldToCompare')
}

describe('Compare Field Validation', () => {
  test('should return a InvalidParamsError if validation fails', () => {
    const sut = makeSut()
    const error = sut.validate({
      field: 'any_value',
      fieldToCompare: 'wrong_value'
    })
    expect(error).toEqual(new InvalidParamError('field'))
  })

  test('should not return if validation success', () => {
    const sut = makeSut()
    const error = sut.validate({
      field: 'any_value',
      fieldToCompare: 'any_value'
    })
    expect(error).toBeFalsy()
  })
})
