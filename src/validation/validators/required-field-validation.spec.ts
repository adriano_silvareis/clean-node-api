import { MissingParamError } from '@/presentation/error'

import { RequiredFieldValidation } from './required-field-validation'

function makeSut (): RequiredFieldValidation {
  return new RequiredFieldValidation('field')
}

describe('Required Field Validation', () => {
  test('should return a MissingParams if validation fails', () => {
    const sut = makeSut()
    const error = sut.validate({ name: 'any_name' })
    expect(error).toEqual(new MissingParamError('field'))
  })

  test('should not return if validation success', () => {
    const sut = makeSut()
    const error = sut.validate({ field: 'any_name' })
    expect(error).toBeFalsy()
  })
})
