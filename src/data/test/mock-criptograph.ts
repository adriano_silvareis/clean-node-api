import { Encrypter } from '@/data//protocols/criptograph/encrypter'
import { Decrypter } from '@/data/protocols/criptograph/decrypter'
import { HashComparer } from '@/data/protocols/criptograph/hash-comparer'
import { Hasher } from '@/data/protocols/criptograph/hasher'

export function mockHasher (): Hasher {
  class HasherStub implements Hasher {
    async hash (value: string): Promise<string> {
      return Promise.resolve('hashed_password')
    }
  }
  return new HasherStub()
}

export function mockDecrypter (): Decrypter {
  class DecrypterStub implements Decrypter {
    async decrypt (value: string): Promise<string> {
      return Promise.resolve('any_value')
    }
  }
  return new DecrypterStub()
}

export function mockEncrypter (): Encrypter {
  class EncrypterStub implements Encrypter {
    async encrypter (id: string): Promise<string> {
      return Promise.resolve('any_token')
    }
  }
  return new EncrypterStub()
}

export function mockHashComparer (): HashComparer {
  class HashComparerStub implements HashComparer {
    async compare (value: string, hash: string): Promise<boolean> {
      return Promise.resolve(true)
    }
  }
  return new HashComparerStub()
}
