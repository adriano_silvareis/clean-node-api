import { mockSurveyRepository } from '@/data/test'
import { throwError, mockSurveyParams } from '@/domain/test'

import { AddSurveyRepository } from './db-add-account-protocols'
import { DbAddSurvey } from './db-add-surve'

describe('DbAddSurvey UseCase', () => {
  test('should call AddSurveyRepository with correct values', async () => {
    const { sut, addSurveyRepositoryStub } = makeSut()
    const addSpy = jest.spyOn(addSurveyRepositoryStub, 'add')
    await sut.add(mockSurveyParams())
    expect(addSpy).toHaveBeenCalledWith(mockSurveyParams())
  })

  test('should throw if AddSurveyRepository throws', async () => {
    const { sut, addSurveyRepositoryStub } = makeSut()
    jest.spyOn(addSurveyRepositoryStub, 'add').mockImplementationOnce(throwError)
    const promise = sut.add(mockSurveyParams())
    await expect(promise).rejects.toThrow()
  })
})

type SutTypes = {
  sut: DbAddSurvey
  addSurveyRepositoryStub: AddSurveyRepository
}

function makeSut (): SutTypes {
  const addSurveyRepositoryStub = mockSurveyRepository()
  const sut = new DbAddSurvey(addSurveyRepositoryStub)
  return {
    sut,
    addSurveyRepositoryStub
  }
}
