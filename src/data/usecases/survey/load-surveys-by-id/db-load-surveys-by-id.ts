import { LoadSurveyByIdRepository, SurveyModel, LoadSurveysById } from './db-load-surveys-by-id-protocols'
export class DbLoadSurveyById implements LoadSurveysById {
  constructor (private readonly loadSurveyByIdRepository: LoadSurveyByIdRepository) {}

  async loadById (id: string): Promise<SurveyModel | null> {
    const survey = await this.loadSurveyByIdRepository.loadById(id)
    return survey
  }
}
