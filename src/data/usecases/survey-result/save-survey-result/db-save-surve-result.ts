import { SaveSurveyResultParams } from '@/domain/usecases/survey-result/save-survey-result'

import { SaveSurveyResultRepository, SurveyResultModel, SaveSurveyResult } from './db-save-surve-result-protocols'

export class DbSaveSurveyResult implements SaveSurveyResult {
  constructor (private readonly saveSurveyResultRepository: SaveSurveyResultRepository) {}

  async save (data: SaveSurveyResultParams): Promise<SurveyResultModel> {
    const surveyResult = await this.saveSurveyResultRepository.save(data)
    return surveyResult
  }
}
