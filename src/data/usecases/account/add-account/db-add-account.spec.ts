import { mockHasher, mockAddAccountRepository, mockLoadAccountByEmailRepository } from '@/data/test'
import { mockAccountModel, mockAddAccountParams, throwError } from '@/domain/test'

import { DbAddAccount } from './db-add-account'
import { Hasher, AddAccountRepository, LoadAccountByEmailRepository } from './db-add-account-protocols'
type SutTypes = {
  sut: DbAddAccount
  hasherStub: Hasher
  addAccountRepositoryStub: AddAccountRepository
  mockloadAccountByEmailRepository: LoadAccountByEmailRepository
}

function makeSut (): SutTypes {
  const hasherStub = mockHasher()
  const addAccountRepositoryStub = mockAddAccountRepository()
  const mockloadAccountByEmailRepository = mockLoadAccountByEmailRepository()
  jest.spyOn(mockloadAccountByEmailRepository, 'loadByEmail').mockReturnValue(Promise.resolve(null))
  const sut = new DbAddAccount(hasherStub, addAccountRepositoryStub, mockloadAccountByEmailRepository)
  return {
    sut,
    hasherStub,
    addAccountRepositoryStub,
    mockloadAccountByEmailRepository
  }
}

describe('DbAddAccount UseCase', () => {
  test('should call Hasher with correct password', async () => {
    const { sut, hasherStub } = makeSut()
    const encryptSpy = jest.spyOn(hasherStub, 'hash')
    const accountData = mockAddAccountParams()
    await sut.add(accountData)
    expect(encryptSpy).toHaveBeenCalledWith(accountData.password)
  })

  test('should throw if Hasher throws', async () => {
    const { sut, hasherStub } = makeSut()
    jest.spyOn(hasherStub, 'hash').mockImplementationOnce(throwError)
    const promise = sut.add(mockAddAccountParams())
    await expect(promise).rejects.toThrow()
  })

  test('should call AddAccountRepository with correct data', async () => {
    const { sut, addAccountRepositoryStub } = makeSut()
    const addSpy = jest.spyOn(addAccountRepositoryStub, 'add')
    await sut.add(mockAddAccountParams())
    expect(addSpy).toHaveBeenCalledWith({
      name: 'any_name',
      email: 'any_email@email.com',
      password: 'hashed_password'
    })
  })

  test('should throw if AddAccountRepository throws', async () => {
    const { sut, addAccountRepositoryStub } = makeSut()
    jest.spyOn(addAccountRepositoryStub, 'add').mockImplementationOnce(throwError)
    const promise = sut.add(mockAddAccountParams())
    await expect(promise).rejects.toThrow()
  })

  test('should return an account on success', async () => {
    const { sut } = makeSut()
    const account = await sut.add(mockAddAccountParams())
    expect(account).toEqual(mockAccountModel())
  })

  test('should return null if LoadAccountByEmailRepository not returns null', async () => {
    const { sut, mockloadAccountByEmailRepository } = makeSut()
    jest.spyOn(mockloadAccountByEmailRepository, 'loadByEmail').mockReturnValueOnce(Promise.resolve(mockAccountModel()))
    const account = await sut.add(mockAddAccountParams())
    expect(account).toBeNull()
  })

  test('should call LoadAccountByEmailRepository with correct email', async () => {
    const { sut, mockloadAccountByEmailRepository } = makeSut()
    const loadSpy = jest.spyOn(mockloadAccountByEmailRepository, 'loadByEmail')
    await sut.add(mockAccountModel())
    expect(loadSpy).toHaveBeenCalledWith('any_email@email.com')
  })
})
