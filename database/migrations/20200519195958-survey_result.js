'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('survey_result', {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.literal('uuid_generate_v4()'),
        allowNull: false
      },
      survey_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'surveys'
          },
          key: 'id'
        },
        allowNull: false
      },
      account_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'accounts'
          },
          key: 'id'
        },
        allowNull: false
      },
      answer: {
        type: Sequelize.STRING,
        allowNull: false
      },
      date: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('survey_result')
  }
}
