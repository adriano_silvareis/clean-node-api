'use strict'

module.exports = {
  up: async function (queryInterface, Sequelize) {
    await queryInterface.sequelize.query('CREATE EXTENSION if not exists "uuid-ossp";')
  },

  down: async function (queryInterface, Sequelize) {
    await queryInterface.sequelize.query('DROP EXTENSION uuid-ossp;')
  }
}
